## Usage

```bash
$sudo go run gojektest.go
```

## View Script Query

name : v_all_sessions_summary 

Run this query 

select
	extract(date from (TIMESTAMP_SECONDS(visitId))) as date,
	extract(hour from (TIMESTAMP_SECONDS(visitId))) as hour,
	count(distinct fullVisitorId) as Total_Unique_Visitor,
	sum((totalTransactionRevenue*0.000001)*14000) as total_revenue_in_rupiah
	FROM `data-to-insights.ecommerce.all_sessions_raw` 
	group by date,hour 

## Delete Script 

This script to avoid duplicate data
	
Run & schedule this query before running insert query


delete from `wihdatusani.summary.all_sessions_summary`
where (date) = DATE_ADD(CURRENT_DATE(), INTERVAL -1 DAY))	

## Insert Script

This script will insert new data to summary table

Table_name : all_sessions_summary 

Run & create schedule this script after running delete script.

insert into `wihdatusani.summary.all_sessions_summary`
select date, hour, Total_Unique_Visitor, total_revenue_in_rupiah
FROM `wihdatusani.summary.v_all_sessions_summary` 

/* ====== delete this comment (/* */) bellow for insert data -1 day ====== */
/* where (date) = DATE_ADD(CURRENT_DATE(), INTERVAL -1 DAY)) */

/* ====== delete this comment (/* /) bellow for insert data -1183 day ======/
/* where (date) = DATE_ADD(CURRENT_DATE(), INTERVAL -1183 DAY))*/
/* ======-1183 due to data raw in data-to-insights.ecommerce.all_sessions_raw start from 01/08/2016 ====== */
