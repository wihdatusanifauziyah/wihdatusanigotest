package main
import ( 
        "log"
        "context"
        "cloud.google.com/go/bigquery" 
)

func main() {
        ctx := context.Background()
        projectID := "wihdatusani"

        client, err := bigquery.NewClient(ctx, projectID)
        if err != nil {
                log.Println(err)
                return
        }

        q := client.Query(`insert into `+ "`wihdatusani.summary.all_sessions_summary`" +
` select date, hour, Total_Unique_Visitor, total_revenue_in_rupiah FROM ` + "`wihdatusani.summary.v_all_sessions_summary`" + ` where (date) = DATE_ADD(CURRENT_DATE(), INTERV
AL -1178 DAY)`)
        _, errQuery := q.Read(ctx)
        if errQuery != nil {
                log.Println(errQuery)
                return
        }
}
